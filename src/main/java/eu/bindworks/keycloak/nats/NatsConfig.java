package eu.bindworks.keycloak.nats;

import io.nats.client.Options;
import org.keycloak.events.Event;
import org.keycloak.events.admin.AdminEvent;

import java.util.regex.Pattern;

public class NatsConfig {
    private static final Pattern SPECIAL_CHARACTERS = Pattern.compile("[^-_a-zA-Z0-9.]");
    private static final Pattern SPACE = Pattern.compile("\\s");
    private static final Pattern DOT = Pattern.compile("\\.");

    private String subjectPrefix;
    private String url;
    private char[] username;
    private char[] password;
    private char[] token;

    public Options.Builder prepareOptionsBuilder() {
        Options.Builder builder = new Options.Builder();
        if (url != null) {
            builder.server(url);
        }
        if (username != null && password != null) {
            builder.userInfo(username, password);
        }
        if (token != null) {
            builder.token(token);
        }
        builder.connectionName("Keycloak Server");
        return builder;
    }

    public String computeSubject(Event event) {
        //KK.EVENT.CLIENT.<REALM>.<RESULT>.<CLIENT>.<EVENT_TYPE>
        String routingKey = subjectPrefix
                + ".CLIENT"
                + "." + removeDots(event.getRealmId())
                + "." + (event.getError() != null ? "ERROR" : "SUCCESS")
                + "." + removeDots(event.getClientId())
                + "." + event.getType();

        return normalizeKey(routingKey);
    }

    public String computeSubject(AdminEvent event) {
        //KK.EVENT.ADMIN.<REALM>.<RESULT>.<RESOURCE_TYPE>.<OPERATION>
        String routingKey = subjectPrefix
                + ".ADMIN"
                + "." + removeDots(event.getRealmId())
                + "." + (event.getError() != null ? "ERROR" : "SUCCESS")
                + "." + event.getResourceTypeAsString()
                + "." + event.getOperationType().toString();
        return normalizeKey(routingKey);
    }

    private static String normalizeKey(CharSequence stringToNormalize) {
        return SPACE
                .matcher(SPECIAL_CHARACTERS
                        .matcher(stringToNormalize)
                        .replaceAll(""))
                .replaceAll("_");
    }

    private static String removeDots(String str) {
        if (str != null) {
            return DOT.matcher(str).replaceAll("");
        }
        return null;
    }

    public static NatsConfig createFromEnvironment() {
        NatsConfig config = new NatsConfig();
        config.url = System.getenv("KK_NATS_URL");
        config.username = getEnvAsCharArray("KK_NATS_USERNAME");
        config.password = getEnvAsCharArray("KK_NATS_PASSWORD");
        config.token = getEnvAsCharArray("KK_NATS_TOKEN");
        config.subjectPrefix = System.getenv("KK_NATS_SUBJECT_PREFIX");
        if (config.subjectPrefix == null) {
            config.subjectPrefix = "KK.EVENT";
        }
        return config;
    }

    private static char[] getEnvAsCharArray(String env) {
        String str = System.getenv(env);
        return str == null ? null : str.toCharArray();
    }
}
