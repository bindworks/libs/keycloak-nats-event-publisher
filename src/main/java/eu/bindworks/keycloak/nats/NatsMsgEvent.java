package eu.bindworks.keycloak.nats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.keycloak.events.Event;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class NatsMsgEvent extends Event implements Serializable {
    private static final long serialVersionUID = -7367949281581799623L;

    public static NatsMsgEvent create(Event event) {
        NatsMsgEvent msg = new NatsMsgEvent();
        msg.setClientId(event.getClientId());
        msg.setDetails(event.getDetails());
        msg.setError(event.getError());
        msg.setIpAddress(event.getIpAddress());
        msg.setRealmId(event.getRealmId());
        msg.setSessionId(event.getSessionId());
        msg.setTime(event.getTime());
        msg.setType(event.getType());
        msg.setUserId(event.getUserId());
        return msg;
    }
}
