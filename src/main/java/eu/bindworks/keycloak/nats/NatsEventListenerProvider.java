package eu.bindworks.keycloak.nats;

import io.nats.client.Connection;
import org.jboss.logging.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerTransaction;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.util.JsonSerialization;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class NatsEventListenerProvider implements EventListenerProvider {
    private static final Logger log = Logger.getLogger(NatsEventListenerProvider.class);

    private final NatsConfig config;
    private Connection connection;

    private final EventListenerTransaction tx = new EventListenerTransaction(this::publishAdminEvent, this::publishEvent);

    public NatsEventListenerProvider(KeycloakSession session, NatsConfig config, Connection connection) throws InterruptedException {
        this.config = config;
        this.connection = connection;

        session.getTransactionManager().enlistAfterCompletion(tx);
    }

    @Override
    public void close() {
        // intentionally left blank
    }

    @Override
    public void onEvent(Event event) {
        tx.addEvent(event.clone());
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean includeRepresentation) {
        tx.addAdminEvent(adminEvent, includeRepresentation);
    }

    private synchronized void publishEvent(Event event) {
        NatsMsgEvent msg = NatsMsgEvent.create(event);
        String subject = config.computeSubject(event);
        byte[] serialized = serializeToJson(msg);

        log.debug("Publishing to NATS subject " + subject + ": " + new String(serialized, StandardCharsets.UTF_8));

        this.connection.publish(subject, serialized);
    }

    private synchronized void publishAdminEvent(AdminEvent event, boolean includeRepresentation) {
        NatsMsgAdminEvent msg = NatsMsgAdminEvent.create(event);
        String subject = config.computeSubject(event);
        byte[] serialized = serializeToJson(msg);

        log.debug("Publishing to NATS subject " + subject + ": " + new String(serialized, StandardCharsets.UTF_8));

        this.connection.publish(subject, serialized);
    }

    private byte[] serializeToJson(Object obj) {
        try {
            return JsonSerialization.writeValueAsBytes(obj);
        } catch (IOException e) {
            log.error("Could not serialize to JSON", e);
            return new byte[] { };
        }
    }
}
