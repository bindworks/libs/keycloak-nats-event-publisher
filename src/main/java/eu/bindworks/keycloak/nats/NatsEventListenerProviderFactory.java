package eu.bindworks.keycloak.nats;

import io.nats.client.Connection;
import io.nats.client.ConnectionListener;
import io.nats.client.Consumer;
import io.nats.client.ErrorListener;
import io.nats.client.JetStreamSubscription;
import io.nats.client.Message;
import io.nats.client.Nats;
import io.nats.client.Options;
import io.nats.client.support.Status;
import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class NatsEventListenerProviderFactory implements EventListenerProviderFactory {
    private static final Logger log = Logger.getLogger(NatsEventListenerProviderFactory.class);

    private NatsConfig natsConfig;

    private Connection connection;

    @Override
    public void init(Config.Scope scope) {
        natsConfig = NatsConfig.createFromEnvironment();
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
        // intentionally left blank
    }

    @Override
    public synchronized EventListenerProvider create(KeycloakSession keycloakSession) {
        try {
            ensureConnected();
            return new NatsEventListenerProvider(keycloakSession, natsConfig, connection);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while attempting to connect to NATS.", e);
        }
    }

    @Override
    public synchronized void close() {
        if (this.connection == null) {
            return;
        }

        try {
            log.info("Closing connection to NATS server at " + this.connection.getConnectedUrl() + "...");

            this.connection.flush(Duration.ZERO);
            Connection connection = this.connection;
            this.connection = null;
            connection.close();
        } catch (InterruptedException | TimeoutException exception) {
            log.error("Error closing connection to NATS server", exception);
        }
    }

    private synchronized void ensureConnected() {
        if (this.connection != null && this.connection.getStatus() != Connection.Status.CLOSED) {
            return;
        }

        try {
            Options options = natsConfig.prepareOptionsBuilder()
                    .connectionListener(new NatsConnectionListener())
                    .errorListener(new NatsErrorListener())
                    .build();
            log.info("Connecting to NATS server at " + Arrays.toString(options.getServers().toArray()) + "...");
            connection = Nats.connect(options);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Error connecting to nats server", e);
        }
    }

    @Override
    public String getId() {
        return "keycloak-to-nats";
    }

    static class NatsConnectionListener implements ConnectionListener {
        @Override
        public void connectionEvent(Connection conn, Events type) {
            log.info("Connection to " + conn.getConnectedUrl() + " " + type);
        }
    }

    static class NatsErrorListener implements ErrorListener {
        @Override
        public void errorOccurred(Connection conn, String error) {
            log.error("Error in connection to " + conn.getConnectedUrl() + " " + error);
        }

        @Override
        public void exceptionOccurred(Connection conn, Exception exp) {
            log.error("Exception in connection to " + conn.getConnectedUrl() + " " + exp);
        }

        @Override
        public void slowConsumerDetected(Connection conn, Consumer consumer) {
            log.error("Slow consumer detected on " + conn.getConnectedUrl());
        }

        @Override
        public void messageDiscarded(Connection conn, Message msg) {
            log.error("Message discarded on " + conn.getConnectedUrl());
        }

        @Override
        public void heartbeatAlarm(Connection conn, JetStreamSubscription sub, long lastStreamSequence, long lastConsumerSequence) {
            log.error("Heartbeat alarm on " + conn.getConnectedUrl());
        }

        @Override
        public void unhandledStatus(Connection conn, JetStreamSubscription sub, Status status) {
            log.error("Unhandled status on " + conn.getConnectedUrl());
        }

        @Override
        public void flowControlProcessed(Connection conn, JetStreamSubscription sub, String subject, FlowControlSource source) {
            log.error("Flow control processed on " + conn.getConnectedUrl());
        }
    }
}
