# Keycloak NATS events publisher

## Build

To build the distribution package, run

```bash
./scripts/package
```

this will produce `./target/keycloak-nats-event-publisher.tar.gz` which
you can directly extract into the root of your keycloak installation.

## Configuration

Configure your REALM to use `keycloak-to-nats` event logger
